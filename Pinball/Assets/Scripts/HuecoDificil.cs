﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuecoDificil : MonoBehaviour {

    public GameObject bola;
    Vector3 posicion = new Vector3(-2.57f,0.08f,4.13f);
    public Text numeroBolas;
    public AudioSource audio;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        numeroBolas.text = (System.Int32.Parse(numeroBolas.text) + 1).ToString();
        bola.transform.position = posicion;
        bola.GetComponent<Rigidbody>().AddForce(new Vector3(0,0,10f));
        audio.Play();
    }
}
