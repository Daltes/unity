﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Salida : MonoBehaviour {
    Vector3 posicionInicial;
    public GameObject bola;
    public BoxCollider salidaLanzadera;
    public Text numeroBolas;
    public Text record;
    public Text puntosFin;
    public Text puntos;
    public salir salir;
    public repetir repetir;
    public GameObject finJuego;
    public AudioSource audio;
    // Use this for initialization
    void Start () {
        posicionInicial = new Vector3(3.1F,0.08F,-2.648F);
	}
	
	// Update is called once per frame
	void Update () {
        if (repetir.pulsado)
        {
            SceneManager.LoadScene("pinball");
        }
        if (salir.pulsado)
        {
            Application.Quit();
            Debug.Log("pulsado");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        numeroBolas.text = (System.Int32.Parse(numeroBolas.text) - 1).ToString();
        bola.transform.position = posicionInicial;
        salidaLanzadera.GetComponent<BoxCollider>().isTrigger = true;
        audio.Play();
        if (System.Int32.Parse(numeroBolas.text) == 0)
        {
            finJuego.active = true;
            bola.active = false;

            puntosFin.text = puntos.text;
            record.text = PlayerPrefs.GetInt("record").ToString();
            if ((System.Int32.Parse(puntosFin.text)) > (System.Int32.Parse(record.text)))
            {
                record.text = puntosFin.text;
                PlayerPrefs.SetInt("record", (System.Int32.Parse(record.text)));
            }
        }
    }

}
