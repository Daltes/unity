﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rebotar : MonoBehaviour {

    public GameObject bola;
    public Text puntosText;
    public int puntos;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        puntosText.text = (System.Int32.Parse(puntosText.text) + puntos).ToString();
        bola.GetComponent<Rigidbody>().velocity =bola.GetComponent<Rigidbody>().velocity * 1.5f;

    }


}
