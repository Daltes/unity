﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerTile : MonoBehaviour {

    public TileManager tilemanager;
    public Text puntos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        tilemanager.todasBaldosas.Add(tilemanager.CrearBaldosa());

        puntos.text = (System.Int32.Parse(puntos.text) + 1).ToString();


        if (tilemanager.sePuedeBorrar)
        {
            tilemanager.todasBaldosas[0].GetComponent<Rigidbody>().useGravity = true;
            tilemanager.todasBaldosas[0].GetComponent<Rigidbody>().isKinematic = false;
            Destroy(tilemanager.todasBaldosas[0],0.5f);
            tilemanager.todasBaldosas.RemoveAt(0);
            if (tilemanager.todasBaldosas.Count >20)
            {
                for (int i = 0;i<10;i++)
                {
                    Destroy(tilemanager.todasBaldosas[i], 0.5f);
                    tilemanager.todasBaldosas.RemoveAt(i);
                }
            }
        }
    }

}
