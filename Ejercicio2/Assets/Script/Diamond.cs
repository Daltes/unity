﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Diamond : MonoBehaviour {

    public Text puntos;
    public GameObject diamante;
    AudioSource sonido;

    // Use this for initialization
    void Start () {
        sonido = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void OnTriggerEnter(Collider other)
    {
        sonido.Play();
        puntos.text = (System.Int32.Parse(puntos.text) + 10).ToString();
        diamante.transform.GetChild(2).gameObject.GetComponent<Renderer>().enabled = false;
        diamante.transform.GetChild(3).gameObject.GetComponent<Renderer>().enabled = false;
        Destroy(diamante.transform.GetChild(3).gameObject);
    }

}
