﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TileManager : MonoBehaviour {
    public List<GameObject> todasBaldosas;
    public GameObject TilePrefabsLeft;
    public GameObject TilePrefabsTop;
    public GameObject TilePrefabsLeftDiamond;
    public GameObject TilePrefabsTopDiamond;
    public GameObject objectBola;
    Vector3 puntoDeInicio;
    bool siguienteBaldosa;
    private IEnumerator destruir;
    private IEnumerator dejarCaer;
    public MoverBola bola;
    public bool sePuedeBorrar;
    float tiempo = 3;
    public Text puntos;
    public GameObject canvas;
    public Text puntosFin;
    public Text record;
    public salir salir;
    public repetir repetir;
    AudioSource sonido;
    private static TileManager instance;
    public static TileManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<TileManager>();
            }
            return instance;
        }
    }

    // Use this for initialization
    void Start () {
        sonido = GetComponent<AudioSource>();
        puntoDeInicio = new Vector3(-6.5F, 0F, 4.5F);
        siguienteBaldosa = false;
        todasBaldosas = new List<GameObject>();

        for (int i=0; i<10;i++)
        {
            todasBaldosas.Add(CrearBaldosa());
        }

        dejarCaer = dejarCaerObjeto(4.0f);
        StartCoroutine(dejarCaer);
        destruir = destruirObjeto(4.0f);
        StartCoroutine(destruir);

    }
	
	// Update is called once per frame
	void Update () {

        if (bola.transform.position.y < 2.4)
        {
            if (!bola.finJuego) { 
                bola.transform.GetChild(0).transform.SetParent(null);
                bola.finJuego = true;
                canvas.SetActive(true);
                puntosFin.text = puntos.text;
                record.text =  PlayerPrefs.GetInt("record").ToString();
                if ((System.Int32.Parse(puntosFin.text)) > (System.Int32.Parse(record.text)))
                {
                    record.text = puntosFin.text;
                    PlayerPrefs.SetInt("record", (System.Int32.Parse(record.text)));
                }
                sonido.Play();
            }
        }
        if (repetir.pulsado)
        {
            SceneManager.LoadScene("juego");
        }
        if (salir.pulsado)
        {
            Application.Quit();
            Debug.Log("pulsado");
        }
    }

    private IEnumerator destruirObjeto(float waitTime)
    {
        while (true)
        {
                
            if (bola != null)
            {
                if (bola.juegoIniciado)
                {
                    yield return new WaitForSeconds(tiempo + 1);
                   // DestroyImmediate(todasBaldosas[0]);
                   // todasBaldosas.RemoveAt(0);
                    tiempo = -0.1f;
                }
                else
                {
                    yield return new WaitForSeconds(0);
                }
            } 
        }
    }

    private IEnumerator dejarCaerObjeto(float waitTime)
    {
        while (true)
        {
            
            if (bola != null)
            {
                if (bola.juegoIniciado)
                {
                    yield return new WaitForSeconds(tiempo);
                    //DestroyImmediate(todasBaldosas[0].transform.GetChild(0).GetComponent<Collider>());
                    todasBaldosas[0].GetComponent<Rigidbody>().useGravity = true;
                    todasBaldosas[0].GetComponent<Rigidbody>().isKinematic = false;
                    sePuedeBorrar = true;
                    yield break;
                }
                else
                {
                    yield return new WaitForSeconds(0);
                }
            }
        }
    }

    public GameObject CrearBaldosa()
    {
        if (siguienteBaldosa)
        {
            puntoDeInicio = new Vector3(puntoDeInicio.x - 3F, 0F, puntoDeInicio.z);
        }
        else
        {
            puntoDeInicio = new Vector3(puntoDeInicio.x, 0F, puntoDeInicio.z + 3F);
        }

        GameObject go;

        if (Random.value <0.5)
        {
            if (Random.value < 0.1)
            {
                go = Instantiate(TilePrefabsTopDiamond, puntoDeInicio, transform.rotation);

                Diamond diamond = go.transform.GetChild(2).GetComponent<Diamond>();
                diamond.puntos = puntos;
                diamond.diamante = go;
            }
            else
            {
                go = Instantiate(TilePrefabsTop, puntoDeInicio, transform.rotation);
                
            }
            
            siguienteBaldosa = true;
        }
        else
        {
            if (Random.value < 0.1)
            {
                go = Instantiate(TilePrefabsLeftDiamond, puntoDeInicio, transform.rotation);

                Diamond diamond = go.transform.GetChild(2).GetComponent<Diamond>();
                diamond.puntos = puntos;
                diamond.diamante = go;
            }
            else
            {
                go = Instantiate(TilePrefabsLeft, puntoDeInicio, transform.rotation);
                
            }
           
            siguienteBaldosa = false;
        }



        TriggerTile script = go.transform.GetChild(0).GetComponent<TriggerTile>();
        script.tilemanager = this;
        script.puntos = puntos;

        finJuego fin = go.transform.GetChild(1).GetComponent<finJuego>();
        fin.bola = objectBola;
        fin.controBola = bola;
       

        return go;

    }
}
