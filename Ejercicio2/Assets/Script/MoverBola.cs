﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverBola : MonoBehaviour {

    public float speed;
    bool direcion;
    public bool juegoIniciado;
    public bool finJuego;
    AudioSource sonido;
    // Use this for initialization
    void Start () {
        finJuego = false;
        direcion = false;
        juegoIniciado = false;
        sonido = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        if (!juegoIniciado)
        {
           
            if (Input.GetMouseButtonDown(0))
            {
                juegoIniciado = true;
                sonido.Play();
            }
            else
            {
                return;
            }
        }

        if (!finJuego)
        {
            if (Input.GetMouseButtonDown(0))
            {
                direcion = !direcion;
            }

            if (direcion)
            {
                transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
            }
            else
            {
                transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
            }
        }


    }
}
