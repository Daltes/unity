﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonSalir : MonoBehaviour {

    public bool esPortada;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        bool salirApliccacion = Input.GetKey(KeyCode.Escape);

        if (salirApliccacion )
        {
            if (esPortada)
            {
                Application.Quit();
            }
            else
            {
                SceneManager.LoadScene("Portada");
            }
            
        }

	}
}
