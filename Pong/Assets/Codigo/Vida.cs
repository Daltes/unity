﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Vida : MonoBehaviour {

    public Text vidas;
    public Text vidas2;
    public GameObject finJuego;
    static int numVidas;
    static int numVidas2;
    public MoverBola bola;
    public MoverPala pala;
    public MoverPala pala2;
    public AudioSource sonido;
    public bool esUnJugador;
    private string nombrePuntos;

    // Use this for initialization
    void Start () {
        string nombre = SceneManager.GetActiveScene().name;
        esUnJugador = nombre.Equals("UnJugador");
        if (esUnJugador)
        {
            numVidas = 3;
            nombrePuntos = "Vidas : ";
        }
        else
        {
            numVidas = 0;
            numVidas2 = 0;
            nombrePuntos = "Puntos : ";
            vidas2.text = nombrePuntos + numVidas;
        }
        
        vidas.text = nombrePuntos + numVidas;
        
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RestarVidas(bool esPrimerJugador)
    {
        if (esUnJugador)
        {
            numVidas--;
        }
        else
        {
            if (esPrimerJugador)
                numVidas++;
            else
                numVidas2++;
        }

        if (esPrimerJugador)
        {
            vidas.text = nombrePuntos + numVidas;
        }
        else
        {
            vidas2.text = nombrePuntos + numVidas2;
        }
        
        if(esUnJugador)
        if (numVidas == 0)
        {
            sonido.Play();
            pala.DetenerPala();
            finJuego.SetActive(true);
            Invoke("CambiarEscena",2);
            finJuego.SetActive(true);
        }

    }

    public void RestarVidas()
    {
        RestarVidas(true);

    }

    public void CambiarEscena()
    {
        SceneManager.LoadScene("Portada");
    }
}
