﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverBola : MonoBehaviour {

    public float velocidadInicial = 400f;
    public Rigidbody rig;
    public Transform barra;
    bool enJuego;
    Vector3 posicionInicial;
    public ElementoInteractivo patallaCompelta;
    public bool direccionAleatoria;

	// Use this for initialization
	void Start () {
        posicionInicial = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        bool sePuedeIniciarPartida = !enJuego && (Input.GetKey(KeyCode.LeftControl) || patallaCompelta.pulsado);

        if (sePuedeIniciarPartida)
        {
            enJuego = true;

            transform.SetParent(null);

            rig.isKinematic = false;

            float direccion;
            if (direccionAleatoria)
            {
                if ((Random.value >= 0.5))
                {
                    direccion = +1;
                }
                else
                {
                    direccion = -1;
                }
            }
            else
            {
                direccion = -1;
            }

            rig.AddForce(new Vector3(direccion*velocidadInicial, direccion * velocidadInicial, 0));

        }
	}

    public void Reset()
    {
        transform.position = posicionInicial;
        transform.SetParent(barra);
        enJuego = false;
        rig.isKinematic = true;
        rig.AddForce(Vector3.zero);
    }

    public void Reset2Players()
    {
        transform.position = posicionInicial;
        enJuego = false;
        rig.isKinematic = true;
        rig.AddForce(Vector3.zero);
    }

}
