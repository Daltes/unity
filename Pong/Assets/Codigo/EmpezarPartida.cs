﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EmpezarPartida : MonoBehaviour {

    public ElementoInteractivo unJugador;
    public ElementoInteractivo dosJugadores;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       

        if (unJugador == null || dosJugadores == null)
        {
            if (Input.GetButton("Fire1"))
            {
                SceneManager.LoadScene("UnJugador");
            }
        }
        else
        {
            if (unJugador.pulsado)
                SceneManager.LoadScene("UnJugador");
            else if (dosJugadores.pulsado)
            {
                SceneManager.LoadScene("DosJugadores");
            }
        }

	}
}
