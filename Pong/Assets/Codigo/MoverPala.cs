﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverPala : MonoBehaviour
{

    public float velocidad;
    private Vector3 posicionInicial;
    bool moverPala;
    public ElementoInteractivo botonUp;
    public ElementoInteractivo botonDown;
    public string botonArriba;
    public string botonAbajo;

    // Use this for initialization
    void Start()
    {
        posicionInicial = transform.position;
        moverPala = true;

        if (botonArriba.Length == 0)
        {
            botonArriba = "up";
            botonAbajo = "down";
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (moverPala)
        {
            Vector3 posicion = transform.position;

            bool esArribaCorrecto = (botonUp.pulsado || Input.GetKey(botonArriba)) && posicion.y <= 8;
            bool esAbajoCorrecto = (botonDown.pulsado || Input.GetKey(botonAbajo)) && posicion.y >= -8;
            if (esArribaCorrecto || esAbajoCorrecto)
            {

                float impulso;

                if (esArribaCorrecto)
                {
                    impulso = 1f;
                }
                else
                {
                    impulso = -1f;
                }

                transform.Translate(new Vector3(impulso * velocidad * Time.deltaTime, 0, 0));
            }
        }

    }

    public void Reset()
    {
        transform.position = posicionInicial;
    }

    public void DetenerPala()
    {
        moverPala = false;
    }
}
