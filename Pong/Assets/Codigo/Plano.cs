﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plano : MonoBehaviour {

    public Vida vidas;
    public AudioSource sonido;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter()
    {
        sonido.Play();
        if (vidas.esUnJugador)
        {
            vidas.RestarVidas();
            
        }
        else
        {
            vidas.RestarVidas(this.name.Equals("PlaneDerecha"));
            vidas.pala2.Reset();
            
        }
        vidas.pala.Reset();

        if (vidas.esUnJugador)
        {
            vidas.bola.Reset();
        }else
            vidas.bola.Reset2Players();
    }
}
